package com.example.diska_1202164307_si4007_pab_modul3;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class Pembukaan extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembukaan);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Pembukaan.this,MainActivity.class));
                finish();
            }
        }, 5000);

        Log.d("OnCreate", "Ini OnCreate, Splash Screen");
    }
}