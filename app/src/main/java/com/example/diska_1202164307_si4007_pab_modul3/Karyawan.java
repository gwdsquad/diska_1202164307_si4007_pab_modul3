package com.example.diska_1202164307_si4007_pab_modul3;

public class Karyawan {
    private String NamaK;
    private String PekerjaanK;
    private String KelaminK;

    public Karyawan(String namak,String pekerjaank, String kelamink){
        this.NamaK = namak;
        this.PekerjaanK = pekerjaank;
        this.KelaminK = kelamink;
    }

    public String getNamaK(){
        return  NamaK;
    }

    public String getPekerjaanK(){
        return  PekerjaanK;
    }

    public String getKelaminK(){
        return KelaminK;
    }

}
