package com.example.diska_1202164307_si4007_pab_modul3;

import android.support.annotation.DrawableRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Detail extends AppCompatActivity {

    private TextView nama, pekerjaan;
    private ImageView foto;
    private String kelamin, namap, pekerjaanp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        nama = findViewById(R.id.Namax);
        pekerjaan = findViewById(R.id.Pekerjaanx);
        foto = findViewById(R.id.Foto);

        kelamin = getIntent().getStringExtra("kelamin");
        namap = getIntent().getStringExtra("nama");
        pekerjaanp = getIntent().getStringExtra("pekerjaan");

        nama.setText(namap);
        pekerjaan.setText(pekerjaanp);
        switch (kelamin){
            case "Laki-laki" :
                foto.setImageResource(R.drawable.laki_laki);
                break;
            case "Perempuan" :
                foto.setImageResource(R.drawable.perempuan);
                break;
        }
    }
}