package com.example.diska_1202164307_si4007_pab_modul3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Karyawan> Karyawans;
    private AdapterKaryawan AdapterKaryawan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycleview);

        int kolom = getResources().getInteger(R.integer.Kolom);
        recyclerView.setLayoutManager(new GridLayoutManager(this, kolom));

        Karyawans = new ArrayList<>();
        if(savedInstanceState != null) {
            Karyawans.clear();
            for (int i = 0; i < savedInstanceState.getStringArrayList("nama").size(); i++) {
                Karyawans.add(new Karyawan(savedInstanceState.getStringArrayList("nama").get(i),
                        savedInstanceState.getStringArrayList("pekerjaan").get(i),
                        savedInstanceState.getStringArrayList("kelamin").get(i)));
            }
        }
        else{
            init();
        }

        AdapterKaryawan = new AdapterKaryawan(Karyawans, this);
        recyclerView.setAdapter(AdapterKaryawan);

        ItemTouchHelper geser = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int from = viewHolder.getAdapterPosition();
                int to = target.getAdapterPosition();
                Collections.swap(Karyawans, from, to);
                AdapterKaryawan.notifyItemMoved(from, to);
                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                Karyawans.remove(viewHolder.getAdapterPosition());
                AdapterKaryawan.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });
        geser.attachToRecyclerView(recyclerView);
    }

    public void menambah(View view) {
        LayoutInflater LI = LayoutInflater.from(MainActivity.this);
        View Layar_topup = LI.inflate(R.layout.topup, null);

        final AlertDialog.Builder popup = new AlertDialog.Builder(MainActivity.this);
        popup.setView(Layar_topup);

        final EditText nama = Layar_topup.findViewById(R.id.Namax);
        final EditText pekerjaan = Layar_topup.findViewById(R.id.Pekerjaanx);
        final Spinner kelamin = Layar_topup.findViewById(R.id.Kelamin);
        popup.setCancelable(true).setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String namax = nama.getText().toString();
                String pekerjaanx = pekerjaan.getText().toString();
                String kelaminx = kelamin.getSelectedItem().toString();
                Toast pesan = Toast.makeText(MainActivity.this,namax+" "+pekerjaanx+" "+kelaminx, Toast.LENGTH_SHORT);
                pesan.show();
                Karyawans.add(new Karyawan(namax, pekerjaanx, kelaminx));
                AdapterKaryawan.notifyDataSetChanged();
                dialog.dismiss();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog popupbisa = popup.create();

        popupbisa.show();
    }

    @Override
    protected void onSaveInstanceState (Bundle outState){
        ArrayList<String>Nama1 = new ArrayList<>();
        ArrayList<String>Pekerjaan1 = new ArrayList<>();
        ArrayList<String>Kelamin1 = new ArrayList<>();
        for (int i = 0 ; i < Karyawans.size() ; i++){
            Nama1.add(Karyawans.get(i).getNamaK());
            Pekerjaan1.add(Karyawans.get(i).getPekerjaanK());
            Kelamin1.add(Karyawans.get(i).getKelaminK());
        }

        outState.putStringArrayList("nama", Nama1);
        outState.putStringArrayList("pekerjaan", Pekerjaan1);
        outState.putStringArrayList("kelamin" , Kelamin1);
        super.onSaveInstanceState(outState);
    }

    public void init(){
        Karyawans.clear();
        Karyawans.add(new Karyawan("Syifa","Depan","Perempuan"));
        Karyawans.add(new Karyawan("Arizallu","Tengah","Laki-laki"));

    }
}