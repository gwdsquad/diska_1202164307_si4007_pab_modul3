package com.example.diska_1202164307_si4007_pab_modul3;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class AdapterKaryawan extends RecyclerView.Adapter<AdapterKaryawan.ViewHolder> {

    private ArrayList<Karyawan> Karyawans;
    private Context Konteks;

    public AdapterKaryawan(ArrayList<Karyawan> karyawan, Context konteks){
        this.Karyawans = karyawan;
        this.Konteks = konteks;
    }



    @Override
    public AdapterKaryawan.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(Konteks).
                inflate(R.layout.list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int posisi) {
        Karyawan karyawan = Karyawans.get(posisi);
        holder.bindTo(karyawan);
    }

    @Override
    public int getItemCount() {
        return Karyawans.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView namax, pekerjaanx;
        private ImageView fotox;
        private String kelaminx;

        public ViewHolder(View itemView){
            super(itemView);

            namax = itemView.findViewById(R.id.Namax);
            pekerjaanx = itemView.findViewById(R.id.Pekerjaanx);
            fotox = itemView.findViewById(R.id.fotox);
            itemView.setOnClickListener(this);
        }

        void bindTo(Karyawan karyawan){
            namax.setText(karyawan.getNamaK());
            pekerjaanx.setText(karyawan.getPekerjaanK());
            kelaminx = karyawan.getKelaminK();
            switch (karyawan.getKelaminK()){
                case "Laki-laki" :
                    fotox.setImageResource(R.drawable.laki_laki);
                    break;
                case "Perempuan" :
                    fotox.setImageResource(R.drawable.perempuan);
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            Intent detail = new Intent(v.getContext(), Detail.class);
            detail.putExtra("nama", namax.getText().toString());
            detail.putExtra("kelamin", kelaminx);
            detail.putExtra("pekerjaan", pekerjaanx.getText().toString());
            v.getContext().startActivity(detail);
        }
    }
}
